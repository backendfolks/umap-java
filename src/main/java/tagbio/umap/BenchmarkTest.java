/*
 * BSD 3-Clause License
 * Copyright (c) 2017, Leland McInnes, 2019 Tag.bio (Java port).
 * See LICENSE.txt.
 */
package tagbio.umap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class BenchmarkData extends Data {
  BenchmarkData(final String dataFile) throws Exception {
    super(dataFile);
  }

  @Override
  String getName() {
    return "BenchmarkData";
  }
}

/**
 * Tests the corresponding class.
 */
public class BenchmarkTest{

  public static void main(String[] args) {
    try {
      final String dataPath = System.getenv("BENCHMARK_DATA");
      int threads = Integer.parseInt(System.getenv("BENCHMARK_THREADS"));

      Utils.message("Benchmark Test threads: " + threads);
      final Data data = new BenchmarkData(dataPath);
      final Umap umap = new Umap();
      umap.setThreads(threads);
      umap.setVerbose(true);
      Utils.message("Loading data: " + dataPath);
      final float[][] d = data.getData();
      Utils.message("Loading data complete.");
      final long start = System.currentTimeMillis();
      final float[][] matrix = umap.fitTransform(d);
      Utils.message("UMAP time: " + Math.round((System.currentTimeMillis() - start) / 1000.0) + " s");
    } catch (Exception e) {
      e.printStackTrace();
      Utils.message( "Exception " + e.getMessage());
    }
  }
}
